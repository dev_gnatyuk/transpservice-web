import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';

import Menu from './sections/menu';
import PublicSections from './sections';

import './style.css';
class PublicPage extends Component {

    constructor(props){
		super(props);
    }

    render() {
        return(
            <div id="content-public">
                <Container>
                    <Row>
                        <Col xs="12"><Menu /></Col>
                    </Row>
                    <PublicSections />
                </Container>
            </div>
        )
    }
}

export default PublicPage;