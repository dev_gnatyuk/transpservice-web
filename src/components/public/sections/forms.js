import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, CardTitle, CardText, Row, Col } from 'reactstrap';
import * as history from './../../../index';

/*Forms*/
import Login from './forms/login';
import Recovery from './forms/recovery';
import Registration from './forms/registration';

/*Actions*/
import * as AuthorizationAction from './../../../actions/authorizationAction';

class AuthorizationForm extends Component {

    constructor(props){
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
          activeTab: '1'
        };

        if (AuthorizationAction.IsExistToken()){
            history.history.push('/cabinet');
        }
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
      }

    render() {
        return(
        <div id="authorization-form">
            <Nav tabs>
                <NavItem>
                    <NavLink
                        onClick={() => { this.toggle('1'); }}>
                        Login
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink
                        onClick={() => { this.toggle('2'); }}>
                        Recovery password
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink
                        onClick={() => { this.toggle('3'); }}>
                        Registration
                    </NavLink>
                </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                    <Login/>
                </TabPane>
                <TabPane tabId="2">
                    <Recovery/>
                </TabPane>
                <TabPane tabId="3">
                    <Registration/>
                </TabPane>
            </TabContent>
        </div>
        )
    }
}

export default AuthorizationForm;