import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

/*Actions*/
import * as AuthorizationAction from './../../../../actions/authorizationAction';

class Recovery extends Component {

    constructor(props){
        super(props);
        this.state = {
            user: {
                email: ''
            }
        }
    }

    onRecovery(){
        AuthorizationAction.recoveryPassword(this.state.user.email);
    }

    onChangeValue = (value, prop) => {
        this.state[prop] = value;
        this.setState(this.state);
    };

    render() {
        return(
            <div id="info-form">
                <FormGroup>
                    <Label for="exampleEmail">Name</Label>
                    <Input type="email" name="email" id="exampleEmail" onChange={e=> this.onChangeValue(e.target.value,'email')} placeholder="with a placeholder" />
                </FormGroup>
                <Button>Registration</Button>
            </div>
        )
    }
}

export default Recovery;