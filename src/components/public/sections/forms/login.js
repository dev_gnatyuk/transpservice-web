import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import * as history from './../../../../index';

/*Helpers*/
import * as notify from './../../../../helpers/notificationHelper';
/*Actions*/
import * as AuthorizationAction from './../../../../actions/authorizationAction';

class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
            user: {
                email: 'admin@ukr.net',
                password: 'admin1234'
            }
        }
    }

    onChangeValue = (value, prop) => {
        this.state.user[prop] = value;
        this.setState(this.state);
    };

    onLogin(){
        AuthorizationAction.login(this.state.user.email, this.state.user.password).then(ok=>{
            notify.success('Authorized');
            history.history.push('/cabinet');
        }, error=>{
            notify.error(error);
        });
    }

    render() {
        return(
            <div id="info-form">
                <FormGroup>
                    <Label for="exampleEmail">Email</Label>
                    <Input type="email" name="email" onChange={e=> this.onChangeValue(e.target.value,'email')} id="exampleEmail" placeholder="with a placeholder" />
                </FormGroup>
                <FormGroup>
                    <Label for="examplePassword">Password</Label>
                    <Input type="password" name="password" onChange={e=> this.onChangeValue(e.target.value,'password')} id="examplePassword" placeholder="password placeholder" />
                </FormGroup>
                <Button onClick={this.onLogin.bind(this)}>Login</Button>
            </div>
        )
    }
}

export default Login;