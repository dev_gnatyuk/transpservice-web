import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Button, ButtonDropdown, Dropdown, DropdownToggle, DropdownItem, DropdownMenu } from 'reactstrap';
import * as history from './../../../index';
import config from './../../../api/config';

/*Actions*/
import * as AuthorizationAction from './../../../actions/authorizationAction';

class Menu extends Component {

    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false,
          dropdownOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    toggleDropDown() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    signOut(){
        config.delToken();
        history.history.push('/');
    }

    isAuthorize(){
        return AuthorizationAction.IsExistToken();

    }



    render() {
        var sing = !this.isAuthorize() ? <NavLink href="/public/forms">Sign In</NavLink> : 
        <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggleDropDown.bind(this)}>
            <DropdownToggle caret>
                Cabinet
            </DropdownToggle>
            <DropdownMenu>
                <DropdownItem><NavLink href="/cabinet">Admin</NavLink></DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={this.signOut.bind(this)}>Sign Out</DropdownItem>
            </DropdownMenu>
        </Dropdown>;
        
        return(
            <div id="menu">
                <Navbar color="faded" light expand="md">
                    <NavbarBrand href="/">reactstrap</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink href="/public/">Home</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/public/info">Info</NavLink>
                        </NavItem>
                        <NavItem>
                            {sing}
                        </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

export default connect(store => ({ store }))(Menu);