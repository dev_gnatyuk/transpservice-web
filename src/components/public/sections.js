import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Switch , Route , Redirect } from 'react-router';

/*Sections*/
import Info from './sections/infoForm';
import Forms from './sections/forms';
import Hello from './sections/hello';



class PublicSections extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/public" component={Hello}/>
                <Route path="/public/forms" component={Forms}/>
                <Route path="/public/info" component={Info}/>
            </Switch>
        );
    }
}

export default PublicSections;