import React from 'react';
import {connect} from 'react-redux';
import { Table, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Breadcrumb, BreadcrumbItem} from 'reactstrap';
import classnames from 'classnames';

/*Actions*/
import * as BundleAction from './../../../../actions/bundleAction';
import { Link } from 'react-router-dom';


class Bundles extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount(){
        BundleAction.get();
    }

    render() {
        var bundles = this.props.store.bundles.list.length > 0 ? this.props.store.bundles.list.map((bundle, i)=>{
            var editLink = "/cabinet/bundle/" + bundle.id;
            return (
                <tr>
                    <th scope="row">{bundle.User.name}</th>
                    <th>{bundle.description}</th>
                    <td>{bundle.With.name}</td>
                    <td>{bundle.To.name}</td>
                    <td>{bundle.price}</td>
                    <td>{bundle.cost}</td>
                    <td>
                        <Link to={editLink} color="warning">Edit</Link>
                        <Button color="danger">Disable</Button>
                        <Button color="success">Enable</Button>
                    </td>
                </tr>
            );
        }) : null;

        return (
            <div id="cabinet-flights">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Bundles</BreadcrumbItem>
                </Breadcrumb>
                <Link to="/cabinet/bundle" className="btn btn-success">Add Bundle</Link>
                <Table>
                    <thead>
                        <tr>
                            <th>Client</th>
                            <th>Description</th>
                            <th>With</th>
                            <th>To</th>
                            <th>Price</th>
                            <th>Cost</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                            { bundles }
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default connect(store => ({ store }))(Bundles);