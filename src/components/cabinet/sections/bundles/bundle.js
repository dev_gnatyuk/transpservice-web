import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, FormText, Breadcrumb, BreadcrumbItem, ButtonGroup } from 'reactstrap';

/*Helpers*/
import * as notify from './../../../../helpers/notificationHelper';

/*Actions*/
import * as UserAction from './../../../../actions/userAction';
import * as OfficeAction from './../../../../actions/officeAction';
import * as BundleAction from './../../../../actions/bundleAction';


class Bundle extends Component {

    constructor(props){
        super(props);
        this.state = {
            bundle:{
                description: '',
                price: '',
                cost: '',
                status: '',
                UserId: '',
                WithId: '',
                ToId: ''
            }
        };
    }

    componentDidMount () {
        OfficeAction.get();
        UserAction.get();
        if (this.props.match.params.id){
            BundleAction.getById(this.props.match.params.id).then(bundle=>{
                this.state.bundle = bundle;
                this.setState(this.state);
            }, e=>{
                notify.error(e);
            });
        }
    }

    onClickSave (){
        if (this.props.match.params.id){
            BundleAction.modify(this.props.match.params.id, this.state.bundle).then(ok=>{
                notify.success('Bundle saved');
            }, e=>{
                notify.error(e);
            });
        }else{
            BundleAction.add(this.state.bundle).then(ok=>{
                notify.success('Bundle created');
            }, e=>{
                notify.error(e);
            });
        }
    };

    onChangeValue = (value, prop) => {
        this.state.bundle[prop] = value;
        this.setState(this.state);
    };

    render() {
        var clients = this.props.store.users.list.length > 0 ? this.props.store.users.list.map((user, i)=>{
            return (
                <option key={i} value={user.id}>{user.name}</option>
            );
        }) : null;

        var withOffices = this.props.store.offices.list.length > 0 ? this.props.store.offices.list.filter(x=>x.id != this.state.bundle.ToId).map((office, i)=>{
            return (
                <option key={i} value={office.id}>{office.location}</option>
            );
        }) : null;

        var toOffices = this.props.store.offices.list.length > 0 ? this.props.store.offices.list.filter(x=>x.id != this.state.bundle.WithId).map((office, i)=>{
            return (
                <option key={i} value={office.id}>{office.location}</option>
            );
        }) : null;
        var linkName = this.props.match.params.id == null ? 'Create bundle' : 'Edit #'+this.state.bundle.id;
        return(
            <div id="car-form">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet/bundles">Bundles</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{linkName}</BreadcrumbItem>
                </Breadcrumb>
                <FormGroup>
                    <Label for="description">Description</Label>
                    <Input type="textarea" value={this.state.bundle.description} onChange={e => this.onChangeValue(e.target.value, 'description')} name="description" id="description" placeholder="description" />
                </FormGroup>
                <FormGroup>
                    <Label for="clinet">Client</Label>
                    <Input type="select"  value={this.state.bundle.UserId} onChange={e => this.onChangeValue(e.target.value, 'UserId')} name="clinet" id="clinet">
                        {clients}
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="with">With</Label>
                    <Input type="select"  value={this.state.bundle.WithId} onChange={e => this.onChangeValue(e.target.value, 'WithId')} name="with" id="with">
                        {withOffices}
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="to">To</Label>
                    <Input type="select"  value={this.state.bundle.ToId} onChange={e => this.onChangeValue(e.target.value, 'ToId')} name="to" id="to">
                        {toOffices}
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="cost">Cost</Label>
                    <Input type="number" value={this.state.bundle.cost} onChange={e => this.onChangeValue(e.target.value, 'cost')} name="cost" id="cost" placeholder="cost" />
                </FormGroup>
                <FormGroup>
                    <Label for="price">Price</Label>
                    <Input type="number" value={this.state.bundle.price} onChange={e => this.onChangeValue(e.target.value, 'price')} name="price" id="price" placeholder="price" />
                </FormGroup>
                <FormGroup>
                    <Label for="to">Status</Label>
                    <ButtonGroup size="lg">
                        <Button>New</Button>
                        <Button>Shipped</Button>
                        <Button>Finished</Button>
                        <Button>Closed</Button>
                        <Button>Rejected</Button>
                  </ButtonGroup>
                </FormGroup>
                <Button color="success" onClick={this.onClickSave.bind(this)}>Save</Button>
            </div>
        )
    }
}

export default connect(store => ({ store }))(Bundle);