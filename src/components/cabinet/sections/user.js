import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Button, Form, FormGroup, Label, Input, FormText, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import * as notify from './../../../helpers/notificationHelper';
/*Actions*/
import * as UserAction from './../../../actions/userAction';
import * as RoleAction from './../../../actions/roleAction';
import { Link } from 'react-router-dom';

class User extends Component {

    constructor(props){
        super(props);
        this.state = {
            user:{
                name: '',
                email: '',
                role: '',
                password: ''
            }
        };
    }

    componentDidMount () {
        RoleAction.get();
        if (this.props.match.params.id){
            UserAction.getById(this.props.match.params.id).then(user=>{
                this.state.user = user;
                this.setState(this.state);
            }, e=>{
                notify.error(e);
            });
        }
    }

    onClickSave (){
        if (this.props.match.params.id){
            UserAction.modify(this.props.match.params.id, this.state.user).then(ok=>{
                notify.success('User saved');
            }, e=>{
                notify.error(e);
            });
        }else{
            UserAction.add(this.state.user).then(ok=>{
                notify.success('User created');
            }, e=>{
                notify.error(e);
            });
        }
    };

    onChangeValue = (value, prop) => {
        this.state.user[prop] = value;
        this.setState(this.state);
    };

    render() {
        var linkName = this.props.match.params.id == null ? 'Create user' : 'Edit '+this.state.user.name;
        var roles = this.props.store.roles.list.length > 0 ? this.props.store.roles.list.map((role, i)=>{
            return (
                <option key={i} value={role}>{role}</option>
            );
        }) : null;
        return(
            <div id="user-form">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet/users">Users</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{linkName}</BreadcrumbItem>
                </Breadcrumb>
                <FormGroup>
                    <Label for="name">Name</Label>
                    <Input type="text" value={this.state.user.name} onChange={e => this.onChangeValue(e.target.value, 'name')} name="name" id="name" placeholder="with a placeholder" />
                </FormGroup>
                <FormGroup>
                    <Label for="email">Email</Label>
                    <Input type="email" value={this.state.user.email} onChange={e => this.onChangeValue(e.target.value, 'email')} name="email" id="email" placeholder="password placeholder" />
                </FormGroup>
                <FormGroup>
                    <Label for="role">Role</Label>
                    <Input type="select"  value={this.state.user.role} onChange={e => this.onChangeValue(e.target.value, 'role')} name="role" id="role">
                        {roles}
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="examplePassword">Password</Label>
                    <Input type="password" onChange={e => this.onChangeValue(e.target.value, 'password')} name="password" id="examplePassword" placeholder="password placeholder" />
                </FormGroup>
                <Button color="success" onClick={this.onClickSave.bind(this)}>Save</Button>
            </div>
        )
    }
}

export default connect(store => ({ store }))(User);