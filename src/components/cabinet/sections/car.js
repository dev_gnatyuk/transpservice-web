import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, FormText, Breadcrumb, BreadcrumbItem } from 'reactstrap';

/*Helpers*/
import * as notify from './../../../helpers/notificationHelper';

/*Actions*/
import * as CarAction from './../../../actions/carAction';


class Car extends Component {

    constructor(props){
        super(props);
        this.state = {
            car:{
                name: '',
                number: ''
            }
        };
    }

    componentDidMount () {
        if (this.props.match.params.id){
            CarAction.getById(this.props.match.params.id).then(car=>{
                this.state.car = car;
                this.setState(this.state);
            }, e=>{
                notify.error(e);
            });
        }
    }

    onClickSave (){
        if (this.props.match.params.id){
            CarAction.modify(this.props.match.params.id, this.state.car).then(ok=>{
                notify.success('Car saved');
            }, e=>{
                notify.error(e);
            });
        }else{
            CarAction.add(this.state.car).then(ok=>{
                notify.success('Car created');
            }, e=>{
                notify.error(e);
            });
        }
    };

    onChangeValue = (value, prop) => {
        this.state.car[prop] = value;
        this.setState(this.state);
    };

    render() {
        var linkName = this.props.match.params.id == null ? 'Create car' : 'Edit '+this.state.car.name;
        return(
            <div id="car-form">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet/cars">Cars</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{linkName}</BreadcrumbItem>
                </Breadcrumb>
                <FormGroup>
                    <Label for="name">Name</Label>
                    <Input type="text" value={this.state.car.name} onChange={e => this.onChangeValue(e.target.value, 'name')} name="name" id="name" placeholder="name" />
                </FormGroup>
                <FormGroup>
                    <Label for="number">Number</Label>
                    <Input type="text" value={this.state.car.number} onChange={e => this.onChangeValue(e.target.value, 'number')} name="number" id="number" placeholder="number" />
                </FormGroup>
                <Button color="success" onClick={this.onClickSave.bind(this)}>Save</Button>
            </div>
        )
    }
}

export default connect(store => ({ store }))(Car);