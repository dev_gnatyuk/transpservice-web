import React from 'react';
import {connect} from 'react-redux';
import { Table, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Breadcrumb, BreadcrumbItem} from 'reactstrap';
import classnames from 'classnames';

/*Actions*/
import * as CarAction from './../../../actions/carAction';
import { Link } from 'react-router-dom';


class Cars extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount(){
        CarAction.get();
    }

    render() {
        var cars = this.props.store.cars.list.length > 0 ? this.props.store.cars.list.map((car, i)=>{
            var editLink = "/cabinet/car/" + car.id;
            return (
                <tr>
                    <th scope="row">{car.name}</th>
                    <td>{car.number}</td>
                    <td>
                        <Link to={editLink} color="warning">Edit</Link>
                        <Button color="danger">Disable</Button>
                        <Button color="success">Enable</Button>
                    </td>
                </tr>
            );
        }) : null;

        return (
            <div id="cabinet-cars">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Cars</BreadcrumbItem>
                </Breadcrumb>
                <Link to="/cabinet/car" className="btn btn-success">Add car</Link>
                <Table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Number</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                            { cars }
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default connect(store => ({ store }))(Cars);