import React from 'react';
import {connect} from 'react-redux';
import { Table, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Breadcrumb, BreadcrumbItem} from 'reactstrap';

/*Actions*/
import * as OfficeAction from './../../../../actions/officeAction';
import { Link } from 'react-router-dom';


class Offices extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount(){
        OfficeAction.get();
    }

    render() {
        var offices = this.props.store.offices.list.length > 0 ? this.props.store.offices.list.map((office, i)=>{
            var editLink = "/cabinet/office/" + office.id;
            return (
                <tr>
                    <th scope="row">{office.name}</th>
                    <td>{office.location}</td>
                    <td>
                        <Link to={editLink} color="warning">Edit</Link>
                        <Button color="danger">Disable</Button>
                        <Button color="success">Enable</Button>
                    </td>
                </tr>
            );
        }) : null;

        return (
            <div id="cabinet-offices">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Offices</BreadcrumbItem>
                </Breadcrumb>
                <Link to="/cabinet/office" className="btn btn-success">Add office</Link>
                <Table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                            { offices }
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default connect(store => ({ store }))(Offices);