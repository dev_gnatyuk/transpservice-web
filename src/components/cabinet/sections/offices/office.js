import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, FormText, Breadcrumb, BreadcrumbItem } from 'reactstrap';

/*Helpers*/
import * as notify from './../../../../helpers/notificationHelper';

/*Actions*/
import * as OfficeAction from './../../../../actions/officeAction';


class Office extends Component {

    constructor(props){
        super(props);
        this.state = {
            office:{
                name: '',
                location: ''
            }
        };
    }

    componentDidMount () {
        if (this.props.match.params.id){
            OfficeAction.getById(this.props.match.params.id).then(office=>{
                this.state.office = office;
                this.setState(this.state);
            }, e=>{
                notify.error(e);
            });
        }
    }

    onClickSave (){
        if (this.props.match.params.id){
            OfficeAction.modify(this.props.match.params.id, this.state.office).then(ok=>{
                notify.success('Office saved');
            }, e=>{
                notify.error(e);
            });
        }else{
            OfficeAction.add(this.state.office).then(ok=>{
                notify.success('Office created');
            }, e=>{
                notify.error(e);
            });
        }
    };

    onChangeValue = (value, prop) => {
        this.state.office[prop] = value;
        this.setState(this.state);
    };

    render() {
        var linkName = this.props.match.params.id == null ? 'Create office' : 'Edit '+this.state.office.name;
        return(
            <div id="car-form">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet/offices">Offices</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{linkName}</BreadcrumbItem>
                </Breadcrumb>
                <FormGroup>
                    <Label for="name">Name</Label>
                    <Input type="text" value={this.state.office.name} onChange={e => this.onChangeValue(e.target.value, 'name')} name="name" id="name" placeholder="name" />
                </FormGroup>
                <FormGroup>
                    <Label for="location">Location</Label>
                    <Input type="text" value={this.state.office.location} onChange={e => this.onChangeValue(e.target.value, 'location')} name="location" id="location" placeholder="location" />
                </FormGroup>
                <Button color="success" onClick={this.onClickSave.bind(this)}>Save</Button>
            </div>
        )
    }
}

export default connect(store => ({ store }))(Office);