import React from 'react';
import {connect} from 'react-redux';
import { Table, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Breadcrumb, BreadcrumbItem} from 'reactstrap';
import classnames from 'classnames';

/*Actions*/
import * as UserAction from './../../../actions/userAction';
import { Link } from 'react-router-dom';


class Users extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount(){
        UserAction.get();
    }

    render() {
        var users = this.props.store.users.list.length > 0 ? this.props.store.users.list.map((user, i)=>{
            var editLink = "/cabinet/user/" + user.id;
            return (
                <tr>
                    <th scope="row">{user.email}</th>
                    <td>{user.name}</td>
                    <td>{user.role}</td>
                    <td>
                        <Link to={editLink} color="warning">Edit</Link>
                        <Button color="danger">Disable</Button>
                        <Button color="success">Enable</Button>
                    </td>
                </tr>
            );
        }) : null;

        return (
            <div id="cabinet-users">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Users</BreadcrumbItem>
                </Breadcrumb>
                <Link to="/cabinet/user" className="btn btn-success">Add user</Link>
                <Table>
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                            { users }
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default connect(store => ({ store }))(Users);