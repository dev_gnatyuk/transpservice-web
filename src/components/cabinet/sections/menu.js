import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';

class Menu extends Component {

    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }

    render() {
        return(
            <div id="menu">
                <Navbar color="faded" light expand="md">
                    <NavbarBrand href="/">reactstrap</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                        <NavItem>
                            <Link to="/cabinet/cars">Cars</Link>
                        </NavItem>
                        <NavItem>
                            <Link to="/cabinet/users">Users</Link>
                        </NavItem>
                        <NavItem>
                            <Link to="/cabinet/offices">Offices</Link>
                        </NavItem>
                        <NavItem>
                            <Link to="/cabinet/flights">Flights</Link>
                        </NavItem>
                        <NavItem>
                            <Link to="/cabinet/bundles">Bundles</Link>
                        </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

export default connect(store => ({ store }))(Menu);