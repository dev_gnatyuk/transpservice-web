import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, FormText, Breadcrumb, BreadcrumbItem } from 'reactstrap';

/*Helpers*/
import * as notify from './../../../../helpers/notificationHelper';

/*Actions*/
import * as CarAction from './../../../../actions/carAction';
import * as FlightAction from './../../../../actions/flightAction';

/*Views*/
import Transporings from './../transportings/transportings';


class Flight extends Component {

    constructor(props){
        super(props);
        this.state = {
            flight:{
                number: '',
                command: '',
                CarId: '',
                startDate: ''
            }
        };
    }

    componentDidMount () {
        CarAction.get().then(ok=>{
            this.state.flight.CarId = this.props.store.cars.list.length > 0 ? this.props.store.cars.list[0].id : null;
            this.setState(this.state);
        });
        if (this.props.match.params.id){
            FlightAction.getById(this.props.match.params.id).then(flight=>{
                this.state.flight = flight;
                this.setState(this.state);
            }, e=>{
                notify.error(e);
            });
        }
    }

    onClickSave (){
        if (this.props.match.params.id){
            FlightAction.modify(this.props.match.params.id, this.state.flight).then(ok=>{
                notify.success('Flight saved');
            }, e=>{
                notify.error(e);
            });
        }else{
            FlightAction.add(this.state.flight).then(ok=>{
                notify.success('Flight created');
            }, e=>{
                notify.error(e);
            });
        }
    };

    onChangeValue = (value, prop) => {
        this.state.flight[prop] = value;
        this.setState(this.state);
    };

    render() {
        var cars = this.props.store.cars.list.length > 0 ? this.props.store.cars.list.map((car, i)=>{
            return (
                <option key={i} value={car.id}>{car.name} | {car.number}</option>
            );
        }) : null;
        var linkName = this.props.match.params.id == null ? 'Create flight' : 'Edit '+this.state.flight.number;
        return(
            <div className='contents' >
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                        <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                        <BreadcrumbItem><Link to="/cabinet/flights">Flights</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{linkName}</BreadcrumbItem>
                    </Breadcrumb>
                <div className='row'>
                    <div className='col-6' id="flight-form">
                        <FormGroup>
                            <Label for="name">Number</Label>
                            <Input type="text" value={this.state.flight.number} onChange={e => this.onChangeValue(e.target.value, 'number')} name="number" id="number" placeholder="number" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="command">Command</Label>
                            <Input type="text" value={this.state.flight.command} onChange={e => this.onChangeValue(e.target.value, 'command')} name="command" id="command" placeholder="command" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="startDate">Start date</Label>
                            <Input type="date" value={this.state.flight.startDate} onChange={e => this.onChangeValue(e.target.value, 'startDate')} name="startDate" id="startDate" placeholder="startDate" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="car">Car</Label>
                            <Input type="select"  value={this.state.flight.CarId} onChange={e => this.onChangeValue(e.target.value, 'CarId')} name="car" id="car">
                                {cars}
                            </Input>
                        </FormGroup>
                        <Button color="success" onClick={this.onClickSave.bind(this)}>Save</Button>
                    </div>
                    <div hidden={!this.state.flight.id} className='col-6' id="transporting-form">
                        <Transporings flight={this.state.flight}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(store => ({ store }))(Flight);