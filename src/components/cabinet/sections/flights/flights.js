import React from 'react';
import {connect} from 'react-redux';
import { Table, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Breadcrumb, BreadcrumbItem} from 'reactstrap';
import classnames from 'classnames';

/*Actions*/
import * as FlightAction from './../../../../actions/flightAction';
import { Link } from 'react-router-dom';


class Flights extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount(){
        FlightAction.get();
    }

    render() {
        var flights = this.props.store.flights.list.length > 0 ? this.props.store.flights.list.map((flight, i)=>{
            var editLink = "/cabinet/flight/" + flight.id;
            return (
                <tr>
                    <th scope="row">{flight.number}</th>
                    <td>{flight.command}</td>
                    <td>
                        <Link to={editLink} color="warning">Edit</Link>
                        <Button color="danger">Disable</Button>
                        <Button color="success">Enable</Button>
                    </td>
                </tr>
            );
        }) : null;

        return (
            <div id="cabinet-flights">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Flights</BreadcrumbItem>
                </Breadcrumb>
                <Link to="/cabinet/flight" className="btn btn-success">Add flight</Link>
                <Table>
                    <thead>
                        <tr>
                            <th>Number</th>
                            <th>Command</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                            { flights }
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default connect(store => ({ store }))(Flights);