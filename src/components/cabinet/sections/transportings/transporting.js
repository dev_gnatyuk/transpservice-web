import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, FormText, Breadcrumb, BreadcrumbItem, Table } from 'reactstrap';

/*Helpers*/
import * as notify from './../../../../helpers/notificationHelper';

/*Actions*/
import * as OfficeAction from './../../../../actions/officeAction';
import * as FlightAction from './../../../../actions/flightAction';
import * as TransportingAction from './../../../../actions/transportingAction';

/*Views*/
import Transporings from './../transportings/transportings';


class Flight extends Component {

    constructor(props){
        super(props);
        this.state = {
            transporting:{
                WithId: '',
                ToId: '',
                FlightId: this.props.match.params.flightId,
                duration: '',
                relations: []
            },

            bundlesSource: [1,2,3,4,5,6,7,8,9],
        };
    }

    componentDidMount () {
        OfficeAction.get().then(ok=>{
            this.state.transporting.ToId = this.props.store.offices.list.length > 0 ? this.props.store.offices.list.find(x=>x.id != this.state.transporting.WithId).id : null;
            this.state.transporting.WithId = this.props.store.offices.list.length > 0 ? this.props.store.offices.list.find(x=>x.id != this.state.transporting.ToId).id : null;
        });
        if (this.props.match.params.id){
            TransportingAction.getById(this.props.match.params.flightId, this.props.match.params.id).then(transporting=>{
                this.state.transporting = transporting;
                this.state.transporting.relations = [];
                this.setState(this.state);
            }, e=>{
                notify.error(e);
            });
        }
        this.setState(this.state);
    }

    onClickSave (){
        if (this.props.match.params.id){
            TransportingAction.modify(this.props.match.params.flightId, this.props.match.params.id, this.state.transporting).then(ok=>{
                notify.success('Transporing saved');
            }, e=>{
                notify.error(e);
            });
        }else{
            TransportingAction.add(this.props.match.params.flightId, this.state.transporting).then(ok=>{
                notify.success('Transporing created');
            }, e=>{
                notify.error(e);
            });
        }
    };

    onChangeValue = (value, prop) => {
        this.state.transporting[prop] = value;
        this.setState(this.state);
    };

    onSelect(id){
        var index = this.state.transporting.relations.findIndex(x=>x == id);
        if (index != -1)
        {
            delete this.state.transporting.relations[index];
            
        }else{
            this.state.transporting.relations.push(id);
        }

        this.setState(this.state);
    }


    render() {
        var relations = this.state.transporting.relations;
        var bundels = this.state.bundlesSource.map(function (a){
            var isSelect = relations.length > 0 ? relations.find(x=>x == a) ? 'table-success' : '' : '';
            return (
                <tr className={isSelect} onClick={()=>this.onSelect(a,2)}>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>Thornton</td>
                    <td>Thornton</td>
                </tr>)
        }.bind(this));
        var withOffices = this.props.store.offices.list.length > 0 ? this.props.store.offices.list.filter(x=>x.id != this.state.transporting.ToId).map((office, i)=>{
            return (
                <option key={i} value={office.id}>{office.location}</option>
            );
        }) : null;

        var toOffices = this.props.store.offices.list.length > 0 ? this.props.store.offices.list.filter(x=>x.id != this.state.transporting.WithId).map((office, i)=>{
            return (
                <option key={i} value={office.id}>{office.location}</option>
            );
        }) : null;
        var linkName = this.props.match.params.id == null ? 'Create transporing' : 'Edit '+this.state.transporting.ToId;
        var backLink = "/cabinet/flight/" + this.props.match.params.flightId;
        return(
                <div className='row'>
                    <div className='col-md-12'>
                        <Breadcrumb>
                            <BreadcrumbItem><Link to="/">Home</Link></BreadcrumbItem>
                            <BreadcrumbItem><Link to="/cabinet">Cabinet</Link></BreadcrumbItem>
                            <BreadcrumbItem><Link to="/cabinet/flights">Flights</Link></BreadcrumbItem>
                            <BreadcrumbItem><Link to={backLink}>Flights</Link></BreadcrumbItem>
                            <BreadcrumbItem active>{linkName}</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                    <div className='col-md-4' id="transporting-form">
                        <FormGroup>
                            <Label for="with">With</Label>
                            <Input type="select"  value={this.state.transporting.WithId} onChange={e => this.onChangeValue(e.target.value, 'WithId')} name="with" id="with">
                                {withOffices}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="to">To</Label>
                            <Input type="select"  value={this.state.transporting.ToId} onChange={e => this.onChangeValue(e.target.value, 'ToId')} name="to" id="to">
                                {toOffices}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="duration">Duration</Label>
                            <Input type="number"  value={this.state.transporting.duration} onChange={e => this.onChangeValue(e.target.value, 'duration')} name="duration" id="duration"></Input>
                        </FormGroup>
                        <Button color="success" onClick={this.onClickSave.bind(this)}>Save</Button>
                    </div>
                    <div className='col-md-8'>
                        <div className='row'>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Username</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {bundels}
                                    </tbody>
                                </Table>
                            </div>
                    </div>
                </div>
        )
    }
}

export default connect(store => ({ store }))(Flight);