import React from 'react';
import {connect} from 'react-redux';
import { Table, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Breadcrumb, BreadcrumbItem} from 'reactstrap';
import { Link } from 'react-router-dom';

/*Actions*/
import * as OfficeAction from './../../../../actions/officeAction';
import * as TransportingAction from './../../../../actions/transportingAction';



class Transportings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flight: this.props.flight
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.flight.id != this.state.flight.id) {
            this.state.flight = nextProps.flight;
            this.setState(this.state);
            TransportingAction.get(this.state.flight.id);
        }
      }

    render() {
        var transportings = this.props.store.transportings.list.length > 0 ? this.props.store.transportings.list.map((transporting, i)=>{
            var editLink = "/cabinet/flight/" + transporting.FlightId + "/transporting/" + transporting.id;
            return (
                <tr>
                    <td>{transporting.With.name}</td>
                    <td>{transporting.To.name}</td>
                    <td>{transporting.duration}</td>
                    <td>
                        <Link to={editLink} color="warning">Edit</Link>
                        <Button color="danger">Disable</Button>
                        <Button color="success">Enable</Button>
                    </td>
                </tr>
            );
        }) : null;
        var createLink = '/cabinet/flight/'+this.props.flight.id+'/transporting';
        return (
            <div id="cabinet-offices">
                <Link to={createLink} className="btn btn-success">Add transporting</Link>
                <Table>
                    <thead>
                        <tr>
                            <th>With</th>
                            <th>To</th>
                            <th>Duration</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                            { transportings }
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default connect(store => ({ store }))(Transportings);