import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Switch , Route , Redirect } from 'react-router';

/*Sections*/
import Users from './sections/users';
import User from './sections/user';

import Cars from './sections/cars';
import Car from './sections/car';

import Offices from './sections/offices/offices';
import Office from './sections/offices/office';

import Flights from './sections/flights/flights';
import Flight from './sections/flights/flight';

import Bundles from './sections/bundles/bundles';
import Bundle from './sections/bundles/bundle';

import Transportings from './sections/transportings/transportings';
import Transporting from './sections/transportings/transporting';
class CabinetSections extends Component {

    constructor(props){
		super(props);
    }

    render() {
        return (
            <Switch>
                <Route exact path="/cabinet" render={() => (<Redirect to="/cabinet/users" />)} /> 
                <Route exact path="/cabinet/users" component={Users}/>
                <Route exact path="/cabinet/user/:id?" component={User}/>
                <Route exact path="/cabinet/cars" component={Cars}/>
                <Route exact path="/cabinet/car/:id?" component={Car}/>
                
                <Route exact path="/cabinet/offices" component={Offices}/>
                <Route exact path="/cabinet/office/:id?" component={Office}/>

                <Route exact path="/cabinet/flights" component={Flights}/>
                <Route exact path="/cabinet/flight/:id?" component={Flight}/>

                <Route exact path="/cabinet/bundles" component={Bundles}/>
                <Route exact path="/cabinet/bundle/:id?" component={Bundle}/>

                <Route exact path="/cabinet/flight/:flightId/transporting/:id?" component={Transporting}/>
            </Switch> 
        );
    }
}

export default CabinetSections;