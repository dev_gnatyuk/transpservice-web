import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';

import Menu from './sections/menu';
import CabinetSections from './sections';

import './style.css';
class CabinetPage extends Component {

    constructor(props){
		super(props);
    }

    render() {
        return(
            <div id="content-public">
                <Container>
                    <Row>
                        <Col xs="12"><Menu /></Col>
                    </Row>
                    
                    <CabinetSections />
                </Container>
            </div>
        )
    }
}

export default CabinetPage;