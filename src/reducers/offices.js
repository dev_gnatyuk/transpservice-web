import { OFFICES } from './constants';

const initialState = {
  list: []
};

export default function(state = initialState, action){

  switch (action.type) {
        case OFFICES.SET:
            return {...state, list: action.payload };
        default:
            return state;
    }
}