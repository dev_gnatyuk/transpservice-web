export const LOADER = {
    SHOW_LOADER: 'SHOW_LOADER',
    HIDE_LOADER: 'HIDE_LOADER'
  }

  export const USERS = {
    SET: 'USERS'
  }

  export const CARS = {
    SET: 'CARS'
  }

  export const TRANSPORTINGS = {
    SET: 'TRANSPORTINGS'
  }

  export const OFFICES = {
    SET: 'OFFICES'
  }

  export const FLIGHTS = {
    SET: 'FLIGHTS'
  }

  export const BUNDLES = {
    SET: 'BUNDLES'
  }

  export const ROLES = {
    SET: 'ROLES'
  }

  export const USER = {
    SET_USER_TOKEN: 'SET_USER_TOKEN',
    SET: 'USER'
  }