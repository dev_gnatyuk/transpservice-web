import { combineReducers } from 'redux'
import loader from './loader';
import user from './user';
import cars from './cars';
import roles from './roles';
import users from './users';
import offices from './offices';
import flights from './flights';
import bundles from './bundles';
import transportings from './transportings';
/*import folders from './folders';
import models from './models';
import roles from './roles';
import accounts from './accounts';
import paymentPlans from './paymentPlans';
import payments from './payments';*/

//import userModal from './modals/user';
/*import visibilityFilter from './visibilityFilter'*/

export default combineReducers({
  /*modal: combineReducers(
      {
        userModal
      }
    ),*/
  loader,
  user,
  cars,
  roles,
  users,
  offices,
  flights,
  bundles,
  transportings
  /*folders,
  models,
  roles,
  accounts,
  payments,
  paymentPlans*/
})