import { store } from './../index';
import config from './../api/config';
import { USERS } from './../reducers/constants';
import users from './../api/users';

export async function get (){
    let res = await users.get();
    let data = await res.json();
    store.dispatch({ type: USERS.SET, payload: data }); 
}

export async function getById (id){
    let res = await users.getById(id);
    let data = await res.json();
    return data;
}

export async function add (user){
    let res = await users.add(user);
    let data = await res.json();
    return data;
}

export async function modify (id, user){
    let res = await users.modify(id, user);
    let data = await res.json();
    return data;
}