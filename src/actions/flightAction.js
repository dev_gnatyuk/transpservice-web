import config from './../api/config';
import flights from './../api/flights';
import { store } from './../index';
import { FLIGHTS } from './../reducers/constants';

export async function add(office) {
    let res = await flights.add(office);
    var data = await res.json(); 
    return data;
}

export async function modify(id, flight) {
    let res = await flights.modify(id, flight);
    var data = await res.json(); 
    return data;
}

export async function get (){
    let res = await flights.get();
    var data = await res.json(); 
    store.dispatch({ type: FLIGHTS.SET, payload: data }); 
}

export async function getById (id){
    let res = await flights.getById(id);
    return await res.json(); 
}