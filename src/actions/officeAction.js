import config from './../api/config';
import offices from './../api/offices';
import { store } from './../index';
import { OFFICES } from './../reducers/constants';

export async function add(office) {
    let res = await offices.add(office);
    var data = await res.json(); 
    return data;
}

export async function modify(id, office) {
    let res = await offices.modify(id, office);
    var data = await res.json(); 
    return data;
}

export async function get (){
    let res = await offices.get();
    var data = await res.json(); 
    store.dispatch({ type: OFFICES.SET, payload: data }); 
}

export async function getById (id){
    let res = await offices.getById(id);
    return await res.json(); 
}