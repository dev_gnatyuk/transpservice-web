import config from './../api/config';
import transportings from './../api/transportings';
import { store } from './../index';
import { TRANSPORTINGS } from './../reducers/constants';

export async function add(flightId, transporting) {
    let res = await transportings.add(flightId, transporting);
    var data = await res.json(); 
    return data;
}

export async function modify(flightId, id, transporting) {
    let res = await transportings.modify(flightId, id, transporting);
    var data = await res.json(); 
    return data;
}

export async function get (flightId){
    let res = await transportings.get(flightId);
    var data = await res.json(); 
    store.dispatch({ type: TRANSPORTINGS.SET, payload: data }); 
}

export async function getById (flightId, id){
    let res = await transportings.getById(flightId, id);
    return await res.json(); 
}