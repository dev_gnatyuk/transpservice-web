import config from './../api/config';
import cars from './../api/cars';
import { store } from './../index';
import { CARS } from './../reducers/constants';

export async function add(car) {
    let res = await cars.add(car);
    var data = await res.json(); 
    return data;
}

export async function modify(id, car) {
    let res = await cars.modify(id, car);
    var data = await res.json(); 
    return data;
}

export async function get (){
    let res = await cars.get();
    var data = await res.json(); 
    store.dispatch({ type: CARS.SET, payload: data }); 
}

export async function getById (id){
    let res = await cars.getById(id);
    return await res.json(); 
}