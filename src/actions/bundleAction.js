import { store } from './../index';
import config from './../api/config';
import { BUNDLES } from './../reducers/constants';
import bundles from './../api/bundles';

export async function add(bundle) {
    let res = await bundles.add(bundle);
    var data = await res.json(); 
    return data;
}

export async function modify(id, bundle) {
    let res = await bundles.modify(id, bundle);
    var data = await res.json(); 
    return data;
}

export async function get (){
    let res = await bundles.get();
    var data = await res.json(); 
    store.dispatch({ type: BUNDLES.SET, payload: data }); 
}

export async function getById (id){
    let res = await bundles.getById(id);
    return await res.json(); 
}