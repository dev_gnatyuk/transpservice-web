import config from './config';
import req from './req';

const apiRoute = 'bundles';
const fullApiPath = config.apiHost + apiRoute;

class bundles {

    static get(){
        return req.get(fullApiPath);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static add(bundle){
        return req.post(fullApiPath, bundle);
    }

    static modify(id, bundle){
        return req.put(fullApiPath + '/' + id, bundle);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }
}

export default bundles;