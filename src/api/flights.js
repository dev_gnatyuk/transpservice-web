import config from './config';
import req from './req';

const apiRoute = 'flights';
const fullApiPath = config.apiHost + apiRoute;

class flights {

    static get(){
        return req.get(fullApiPath);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static add(flight){
        return req.post(fullApiPath, flight);
    }

    static modify(id, flight){
        return req.put(fullApiPath + '/' + id, flight);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }
}

export default flights;