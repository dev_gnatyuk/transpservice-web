import config from './config';
import req from './req';

const apiRoute = 'security';
const fullApiPath = config.apiHost + apiRoute;

class authorization{
    static login(email, password){
        return req.post(fullApiPath + '/login', {email, password});
    }

    static registration(user){
        return req.post(fullApiPath + '/registration', user);
    }
}

export default authorization;