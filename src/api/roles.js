import config from './config';
import req from './req';

const apiRoute = 'roles';
const fullApiPath = config.apiHost + apiRoute;

class roles {

    static get(){
        return req.get(fullApiPath);
    }
}

export default roles;