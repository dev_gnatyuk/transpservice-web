import config from './config';
import req from './req';

const apiRoute = 'users';
const fullApiPath = config.apiHost + apiRoute;

class users {

    static get(){
        return req.get(fullApiPath);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static getCurrent(){
        return req.get(fullApiPath + '/current');
    }

    static add(user){
        return req.post(fullApiPath, user);
    }

    static modify(id, user){
        return req.put(fullApiPath + '/' + id, user);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }
}

export default users;