import config from './config';
import req from './req';

const apiRoute = 'cars';
const fullApiPath = config.apiHost + apiRoute;

class cars {

    static get(){
        return req.get(fullApiPath);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static add(car){
        return req.post(fullApiPath, car);
    }

    static modify(id, car){
        return req.put(fullApiPath + '/' + id, car);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }
}

export default cars;