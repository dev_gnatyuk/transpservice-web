import config from './config';
import req from './req';

const apiRoute = 'offices';
const fullApiPath = config.apiHost + apiRoute;

class offices {

    static get(){
        return req.get(fullApiPath);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static add(office){
        return req.post(fullApiPath, office);
    }

    static modify(id, office){
        return req.put(fullApiPath + '/' + id, office);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }
}

export default offices;