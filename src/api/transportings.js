import config from './config';
import req from './req';

const apiRoute = 'transportings';
const fullApiPath = config.apiHost + apiRoute;

class transportings {

    static get(flightId){
        return req.get(fullApiPath + '/' + flightId);
    }

    static getById(flightId, id){
        return req.get(fullApiPath + '/' + flightId + '/' + id);
    }

    static add(flightId, transporting){
        return req.post(fullApiPath + '/' + flightId, transporting);
    }

    static modify(flightId, id, transporting){
        return req.put(fullApiPath + '/' + flightId + '/' + id, transporting);
    }

    static delete(flightId, id){
        return req.delete(fullApiPath + '/' + flightId + '/' + id);
    }
}

export default transportings;